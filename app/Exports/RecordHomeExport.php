<?php

namespace App\Exports;

use App\Models\RecorHome;
use App\Traits\MetaData;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RecordHomeExport implements FromCollection,WithHeadings,WithMapping
{
    use MetaData;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return RecorHome::all();
    }

    public function headings(): array
    {
        return [
            'Persona',
            'Documento',
            'Barrio',
            'Telefono',
            'Dirección',
            'Estado de salud',
            'Madre Cabeza de familia',
            'Afectaciones',
            'Ayudas'

        ];
    }

    public function map($record): array
    {
        $affectations='';
        $person=$this->simpleFormat($record->person);
        foreach ($record->affectationType as $affectation)
        {
            $affectations.=$affectation->name.',';
        }
        $helps='';
        foreach ($record->help as $ayuda)
        {
            $helps.=$ayuda->name.',';
        }
        $cabeza="no";
        if (isset($person->metas['cabeza_familia'])){
            if($person->metas['cabeza_familia']==1)$cabeza="Si";
        }
        return [

            $record->person->name.' '.$record->person->lastname,
            $record->person->dni,
            $record->neighborhood->name,
            (isset($person->metas['telefono']))??'',
            $person->metas['direccion']??'',
            $person->metas['salud']??'',
            $cabeza,
            $affectations,
            $helps
        ];
    }
}
