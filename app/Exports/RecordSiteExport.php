<?php

namespace App\Exports;

use App\Models\Recordsite;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RecordSiteExport implements FromCollection,WithHeadings,WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Recordsite::all();
    }

    public function headings(): array
    {
        return [

            'Barrio',
            'ID_KEY',
            'Fecha',
            'Caracteristicas de lugar',
            'nivel de alerta',
            'duracion',
            '# Familias',
            '# viviendas',
            '# Escuelas',
            '# Hospitales',
            '# personas afectadas',
            'Zona de riesgo',
            'Tipo de suelo',
            '# personas Heridas',
            '# personas muertas',
            'Observaciones',
            'Tipos Afectaciones',
            'Afectación geografica',
            'Afectación ambiental',
            'Afectación social',
            'Afectación emergencia',
            'Afectación institucional',
            'Afectación territorial',



        ];
    }

    public function map($record): array
    {
        $affectations='';
        foreach ($record->affectationType as $affectation)
        {
            $affectations.=$affectation->name.',';
        }
        return [

            $record->neighborhood->name,
            $record->neighborhood->ID_KEY,
            $record->date->format('Y-m-d'),
            $record->characteristicPlace->name,
            $record->alertLevel->name,
            $record->duration,
            $record->n_families,
            $record->n_homes,
            $record->n_schools,
            $record->n_healthcares,
            $record->n_person_affecteds,
            $record->riskZone->name,
            $record->floorType->name,
            $record->n_person_wounds,
            $record->n_person_deads,
            $record->observations,
            $affectations,
            $record->rating->where('raiting_type',1)->first()->name,
            $record->rating->where('raiting_type',2)->first()->name,
            $record->rating->where('raiting_type',3)->first()->name,
            $record->rating->where('raiting_type',4)->first()->name,
            $record->rating->where('raiting_type',5)->first()->name,
            $record->rating->where('raiting_type',6)->first()->name,
        ];
    }
}
