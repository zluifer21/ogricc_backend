<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAffectationTypeAPIRequest;
use App\Http\Requests\API\UpdateAffectationTypeAPIRequest;
use App\Models\AffectationType;
use App\Repositories\AffectationTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AffectationTypeController
 * @package App\Http\Controllers\API
 */

class AffectationTypeAPIController extends AppBaseController
{
    /** @var  AffectationTypeRepository */
    private $affectationTypeRepository;

    public function __construct(AffectationTypeRepository $affectationTypeRepo)
    {
        $this->affectationTypeRepository = $affectationTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/affectationTypes",
     *      summary="Get a listing of the AffectationTypes.",
     *      tags={"AffectationType"},
     *      description="Get all AffectationTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/AffectationType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $affectationTypes = $this->affectationTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($affectationTypes->toArray(), 'Affectation Types retrieved successfully');
    }

    /**
     * @param CreateAffectationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/affectationTypes",
     *      summary="Store a newly created AffectationType in storage",
     *      tags={"AffectationType"},
     *      description="Store AffectationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AffectationType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AffectationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AffectationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAffectationTypeAPIRequest $request)
    {
        $input = $request->all();

        $affectationType = $this->affectationTypeRepository->create($input);

        return $this->sendResponse($affectationType->toArray(), 'Affectation Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/affectationTypes/{id}",
     *      summary="Display the specified AffectationType",
     *      tags={"AffectationType"},
     *      description="Get AffectationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AffectationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AffectationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var AffectationType $affectationType */
        $affectationType = $this->affectationTypeRepository->find($id);

        if (empty($affectationType)) {
            return $this->sendError('Affectation Type not found');
        }

        return $this->sendResponse($affectationType->toArray(), 'Affectation Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAffectationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/affectationTypes/{id}",
     *      summary="Update the specified AffectationType in storage",
     *      tags={"AffectationType"},
     *      description="Update AffectationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AffectationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AffectationType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AffectationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AffectationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAffectationTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var AffectationType $affectationType */
        $affectationType = $this->affectationTypeRepository->find($id);

        if (empty($affectationType)) {
            return $this->sendError('Affectation Type not found');
        }

        $affectationType = $this->affectationTypeRepository->update($input, $id);

        return $this->sendResponse($affectationType->toArray(), 'AffectationType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/affectationTypes/{id}",
     *      summary="Remove the specified AffectationType from storage",
     *      tags={"AffectationType"},
     *      description="Delete AffectationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AffectationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var AffectationType $affectationType */
        $affectationType = $this->affectationTypeRepository->find($id);

        if (empty($affectationType)) {
            return $this->sendError('Affectation Type not found');
        }

        $affectationType->delete();

        return $this->sendSuccess('Affectation Type deleted successfully');
    }
}
