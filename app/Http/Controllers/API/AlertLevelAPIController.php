<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAlertLevelAPIRequest;
use App\Http\Requests\API\UpdateAlertLevelAPIRequest;
use App\Models\AlertLevel;
use App\Repositories\AlertLevelRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AlertLevelController
 * @package App\Http\Controllers\API
 */

class AlertLevelAPIController extends AppBaseController
{
    /** @var  AlertLevelRepository */
    private $alertLevelRepository;

    public function __construct(AlertLevelRepository $alertLevelRepo)
    {
        $this->alertLevelRepository = $alertLevelRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/alertLevels",
     *      summary="Get a listing of the AlertLevels.",
     *      tags={"AlertLevel"},
     *      description="Get all AlertLevels",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/AlertLevel")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $alertLevels = $this->alertLevelRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($alertLevels->toArray(), 'Alert Levels retrieved successfully');
    }

    /**
     * @param CreateAlertLevelAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/alertLevels",
     *      summary="Store a newly created AlertLevel in storage",
     *      tags={"AlertLevel"},
     *      description="Store AlertLevel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AlertLevel that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AlertLevel")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AlertLevel"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAlertLevelAPIRequest $request)
    {
        $input = $request->all();

        $alertLevel = $this->alertLevelRepository->create($input);

        return $this->sendResponse($alertLevel->toArray(), 'Alert Level saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/alertLevels/{id}",
     *      summary="Display the specified AlertLevel",
     *      tags={"AlertLevel"},
     *      description="Get AlertLevel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AlertLevel",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AlertLevel"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var AlertLevel $alertLevel */
        $alertLevel = $this->alertLevelRepository->find($id);

        if (empty($alertLevel)) {
            return $this->sendError('Alert Level not found');
        }

        return $this->sendResponse($alertLevel->toArray(), 'Alert Level retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAlertLevelAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/alertLevels/{id}",
     *      summary="Update the specified AlertLevel in storage",
     *      tags={"AlertLevel"},
     *      description="Update AlertLevel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AlertLevel",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AlertLevel that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AlertLevel")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AlertLevel"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAlertLevelAPIRequest $request)
    {
        $input = $request->all();

        /** @var AlertLevel $alertLevel */
        $alertLevel = $this->alertLevelRepository->find($id);

        if (empty($alertLevel)) {
            return $this->sendError('Alert Level not found');
        }

        $alertLevel = $this->alertLevelRepository->update($input, $id);

        return $this->sendResponse($alertLevel->toArray(), 'AlertLevel updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/alertLevels/{id}",
     *      summary="Remove the specified AlertLevel from storage",
     *      tags={"AlertLevel"},
     *      description="Delete AlertLevel",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AlertLevel",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var AlertLevel $alertLevel */
        $alertLevel = $this->alertLevelRepository->find($id);

        if (empty($alertLevel)) {
            return $this->sendError('Alert Level not found');
        }

        $alertLevel->delete();

        return $this->sendSuccess('Alert Level deleted successfully');
    }
}
