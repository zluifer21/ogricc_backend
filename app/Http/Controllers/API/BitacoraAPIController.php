<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBitacoraAPIRequest;
use App\Http\Requests\API\UpdateBitacoraAPIRequest;
use App\Models\Bitacora;
use App\Repositories\BitacoraRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BitacoraController
 * @package App\Http\Controllers\API
 */

class BitacoraAPIController extends AppBaseController
{
    /** @var  BitacoraRepository */
    private $bitacoraRepository;

    public function __construct(BitacoraRepository $bitacoraRepo)
    {
        $this->bitacoraRepository = $bitacoraRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/bitacoras",
     *      summary="Get a listing of the Bitacoras.",
     *      tags={"Bitacora"},
     *      description="Get all Bitacoras",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Bitacora")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $bitacoras = $this->bitacoraRepository->index();

        return $this->sendResponse($bitacoras->toArray(), 'Bitacoras retrieved successfully');
    }

    /**
     * @param CreateBitacoraAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/bitacoras",
     *      summary="Store a newly created Bitacora in storage",
     *      tags={"Bitacora"},
     *      description="Store Bitacora",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bitacora that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bitacora")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bitacora"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBitacoraAPIRequest $request)
    {
        $input = $request->all();

        $bitacora = $this->bitacoraRepository->store($input);

        return $this->sendResponse($bitacora->toArray(), 'Bitacora saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/bitacoras/{id}",
     *      summary="Display the specified Bitacora",
     *      tags={"Bitacora"},
     *      description="Get Bitacora",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bitacora",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bitacora"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Bitacora $bitacora */
        $bitacora = $this->bitacoraRepository->find($id);

        if (empty($bitacora)) {
            return $this->sendError('Bitacora not found');
        }

        return $this->sendResponse($bitacora->toArray(), 'Bitacora retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBitacoraAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bitacoras/{id}",
     *      summary="Update the specified Bitacora in storage",
     *      tags={"Bitacora"},
     *      description="Update Bitacora",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bitacora",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bitacora that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bitacora")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bitacora"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBitacoraAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bitacora $bitacora */
        $bitacora = $this->bitacoraRepository->find($id);

        if (empty($bitacora)) {
            return $this->sendError('Bitacora not found');
        }

        $bitacora = $this->bitacoraRepository->update($input, $id);

        return $this->sendResponse($bitacora->toArray(), 'Bitacora updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bitacoras/{id}",
     *      summary="Remove the specified Bitacora from storage",
     *      tags={"Bitacora"},
     *      description="Delete Bitacora",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bitacora",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Bitacora $bitacora */
        $bitacora = $this->bitacoraRepository->find($id);

        if (empty($bitacora)) {
            return $this->sendError('Bitacora not found');
        }

        $bitacora->delete();

        return $this->sendSuccess('Bitacora deleted successfully');
    }
}
