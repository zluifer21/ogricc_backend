<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCharacteristicPlaceAPIRequest;
use App\Http\Requests\API\UpdateCharacteristicPlaceAPIRequest;
use App\Models\CharacteristicPlace;
use App\Repositories\CharacteristicPlaceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CharacteristicPlaceController
 * @package App\Http\Controllers\API
 */

class CharacteristicPlaceAPIController extends AppBaseController
{
    /** @var  CharacteristicPlaceRepository */
    private $characteristicPlaceRepository;

    public function __construct(CharacteristicPlaceRepository $characteristicPlaceRepo)
    {
        $this->characteristicPlaceRepository = $characteristicPlaceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/characteristicPlaces",
     *      summary="Get a listing of the CharacteristicPlaces.",
     *      tags={"CharacteristicPlace"},
     *      description="Get all CharacteristicPlaces",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CharacteristicPlace")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $characteristicPlaces = $this->characteristicPlaceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($characteristicPlaces->toArray(), 'Characteristic Places retrieved successfully');
    }

    /**
     * @param CreateCharacteristicPlaceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/characteristicPlaces",
     *      summary="Store a newly created CharacteristicPlace in storage",
     *      tags={"CharacteristicPlace"},
     *      description="Store CharacteristicPlace",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CharacteristicPlace that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CharacteristicPlace")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CharacteristicPlace"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCharacteristicPlaceAPIRequest $request)
    {
        $input = $request->all();

        $characteristicPlace = $this->characteristicPlaceRepository->create($input);

        return $this->sendResponse($characteristicPlace->toArray(), 'Characteristic Place saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/characteristicPlaces/{id}",
     *      summary="Display the specified CharacteristicPlace",
     *      tags={"CharacteristicPlace"},
     *      description="Get CharacteristicPlace",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CharacteristicPlace",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CharacteristicPlace"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CharacteristicPlace $characteristicPlace */
        $characteristicPlace = $this->characteristicPlaceRepository->find($id);

        if (empty($characteristicPlace)) {
            return $this->sendError('Characteristic Place not found');
        }

        return $this->sendResponse($characteristicPlace->toArray(), 'Characteristic Place retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCharacteristicPlaceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/characteristicPlaces/{id}",
     *      summary="Update the specified CharacteristicPlace in storage",
     *      tags={"CharacteristicPlace"},
     *      description="Update CharacteristicPlace",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CharacteristicPlace",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CharacteristicPlace that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CharacteristicPlace")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CharacteristicPlace"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCharacteristicPlaceAPIRequest $request)
    {
        $input = $request->all();

        /** @var CharacteristicPlace $characteristicPlace */
        $characteristicPlace = $this->characteristicPlaceRepository->find($id);

        if (empty($characteristicPlace)) {
            return $this->sendError('Characteristic Place not found');
        }

        $characteristicPlace = $this->characteristicPlaceRepository->update($input, $id);

        return $this->sendResponse($characteristicPlace->toArray(), 'CharacteristicPlace updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/characteristicPlaces/{id}",
     *      summary="Remove the specified CharacteristicPlace from storage",
     *      tags={"CharacteristicPlace"},
     *      description="Delete CharacteristicPlace",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CharacteristicPlace",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CharacteristicPlace $characteristicPlace */
        $characteristicPlace = $this->characteristicPlaceRepository->find($id);

        if (empty($characteristicPlace)) {
            return $this->sendError('Characteristic Place not found');
        }

        $characteristicPlace->delete();

        return $this->sendSuccess('Characteristic Place deleted successfully');
    }
}
