<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Bitacora;
use App\Models\Recordsite;
use App\Models\RecorHome;
use App\Repositories\RecordsiteRepository;
use App\Repositories\RecorHomeRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Response;

class DashboarController extends  AppBaseController
{
    private $recorHomeRepository;
    private $recordsiteRepository;

    public function __construct(RecorHomeRepository $recorHomeRepo , RecordsiteRepository $recordsiteRepo)
    {
        $this->recorHomeRepository = $recorHomeRepo;
        $this->recordsiteRepository = $recordsiteRepo;
    }
    public function index()
    {

        $data = [];
        $recordsites= Recordsite::whereDate('created_at', Carbon::today())->count();
        $recordhomes= RecorHome::whereDate('created_at', Carbon::today())->count();
        $bitacoras= Bitacora::whereDate('created_at', Carbon::today())->count();
        array_push($data,self::addResponse(2,"Actas de sitio",$recordsites));
        array_push($data,self::addResponse(1,"Actas de visita",$recordhomes));
        array_push($data,self::addResponse(3,"Bitacora",$bitacoras));
        $data['data_homes']=$this->recorHomeRepository->chart();
        $data['data_sites']=$this->recordsiteRepository->chart();

        return $this->sendResponse($data, 'Dashboard');
    }

    public static function addResponse($color,$text,$total){
    $data = new class (){};
    $data->color=$color;
    $data->text=$text;
    $data->total=$total;
    return $data;
}
}
