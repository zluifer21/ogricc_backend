<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFloorTypeAPIRequest;
use App\Http\Requests\API\UpdateFloorTypeAPIRequest;
use App\Models\FloorType;
use App\Repositories\FloorTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class FloorTypeController
 * @package App\Http\Controllers\API
 */

class FloorTypeAPIController extends AppBaseController
{
    /** @var  FloorTypeRepository */
    private $floorTypeRepository;

    public function __construct(FloorTypeRepository $floorTypeRepo)
    {
        $this->floorTypeRepository = $floorTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/floorTypes",
     *      summary="Get a listing of the FloorTypes.",
     *      tags={"FloorType"},
     *      description="Get all FloorTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/FloorType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $floorTypes = $this->floorTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($floorTypes->toArray(), 'Floor Types retrieved successfully');
    }

    /**
     * @param CreateFloorTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/floorTypes",
     *      summary="Store a newly created FloorType in storage",
     *      tags={"FloorType"},
     *      description="Store FloorType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FloorType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FloorType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FloorType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFloorTypeAPIRequest $request)
    {
        $input = $request->all();

        $floorType = $this->floorTypeRepository->create($input);

        return $this->sendResponse($floorType->toArray(), 'Floor Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/floorTypes/{id}",
     *      summary="Display the specified FloorType",
     *      tags={"FloorType"},
     *      description="Get FloorType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FloorType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FloorType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var FloorType $floorType */
        $floorType = $this->floorTypeRepository->find($id);

        if (empty($floorType)) {
            return $this->sendError('Floor Type not found');
        }

        return $this->sendResponse($floorType->toArray(), 'Floor Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFloorTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/floorTypes/{id}",
     *      summary="Update the specified FloorType in storage",
     *      tags={"FloorType"},
     *      description="Update FloorType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FloorType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FloorType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FloorType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FloorType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFloorTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var FloorType $floorType */
        $floorType = $this->floorTypeRepository->find($id);

        if (empty($floorType)) {
            return $this->sendError('Floor Type not found');
        }

        $floorType = $this->floorTypeRepository->update($input, $id);

        return $this->sendResponse($floorType->toArray(), 'FloorType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/floorTypes/{id}",
     *      summary="Remove the specified FloorType from storage",
     *      tags={"FloorType"},
     *      description="Delete FloorType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FloorType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var FloorType $floorType */
        $floorType = $this->floorTypeRepository->find($id);

        if (empty($floorType)) {
            return $this->sendError('Floor Type not found');
        }

        $floorType->delete();

        return $this->sendSuccess('Floor Type deleted successfully');
    }
}
