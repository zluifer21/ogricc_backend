<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHomeStatusAPIRequest;
use App\Http\Requests\API\UpdateHomeStatusAPIRequest;
use App\Models\HomeStatus;
use App\Repositories\HomeStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class HomeStatusController
 * @package App\Http\Controllers\API
 */

class HomeStatusAPIController extends AppBaseController
{
    /** @var  HomeStatusRepository */
    private $homeStatusRepository;

    public function __construct(HomeStatusRepository $homeStatusRepo)
    {
        $this->homeStatusRepository = $homeStatusRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/homeStatuses",
     *      summary="Get a listing of the HomeStatuses.",
     *      tags={"HomeStatus"},
     *      description="Get all HomeStatuses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/HomeStatus")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $homeStatuses = $this->homeStatusRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($homeStatuses->toArray(), 'Home Statuses retrieved successfully');
    }

    /**
     * @param CreateHomeStatusAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/homeStatuses",
     *      summary="Store a newly created HomeStatus in storage",
     *      tags={"HomeStatus"},
     *      description="Store HomeStatus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="HomeStatus that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/HomeStatus")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/HomeStatus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateHomeStatusAPIRequest $request)
    {
        $input = $request->all();

        $homeStatus = $this->homeStatusRepository->create($input);

        return $this->sendResponse($homeStatus->toArray(), 'Home Status saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/homeStatuses/{id}",
     *      summary="Display the specified HomeStatus",
     *      tags={"HomeStatus"},
     *      description="Get HomeStatus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of HomeStatus",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/HomeStatus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var HomeStatus $homeStatus */
        $homeStatus = $this->homeStatusRepository->find($id);

        if (empty($homeStatus)) {
            return $this->sendError('Home Status not found');
        }

        return $this->sendResponse($homeStatus->toArray(), 'Home Status retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateHomeStatusAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/homeStatuses/{id}",
     *      summary="Update the specified HomeStatus in storage",
     *      tags={"HomeStatus"},
     *      description="Update HomeStatus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of HomeStatus",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="HomeStatus that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/HomeStatus")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/HomeStatus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateHomeStatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var HomeStatus $homeStatus */
        $homeStatus = $this->homeStatusRepository->find($id);

        if (empty($homeStatus)) {
            return $this->sendError('Home Status not found');
        }

        $homeStatus = $this->homeStatusRepository->update($input, $id);

        return $this->sendResponse($homeStatus->toArray(), 'HomeStatus updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/homeStatuses/{id}",
     *      summary="Remove the specified HomeStatus from storage",
     *      tags={"HomeStatus"},
     *      description="Delete HomeStatus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of HomeStatus",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var HomeStatus $homeStatus */
        $homeStatus = $this->homeStatusRepository->find($id);

        if (empty($homeStatus)) {
            return $this->sendError('Home Status not found');
        }

        $homeStatus->delete();

        return $this->sendSuccess('Home Status deleted successfully');
    }
}
