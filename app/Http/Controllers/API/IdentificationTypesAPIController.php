<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIdentificationTypesAPIRequest;
use App\Http\Requests\API\UpdateIdentificationTypesAPIRequest;
use App\Models\IdentificationTypes;
use App\Repositories\IdentificationTypesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class IdentificationTypesController
 * @package App\Http\Controllers\API
 */

class IdentificationTypesAPIController extends AppBaseController
{
    /** @var  IdentificationTypesRepository */
    private $identificationTypesRepository;

    public function __construct(IdentificationTypesRepository $identificationTypesRepo)
    {
        $this->identificationTypesRepository = $identificationTypesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/identificationTypes",
     *      summary="Get a listing of the IdentificationTypes.",
     *      tags={"IdentificationTypes"},
     *      description="Get all IdentificationTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/IdentificationTypes")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $identificationTypes = $this->identificationTypesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($identificationTypes->toArray(), 'Identification Types retrieved successfully');
    }

    /**
     * @param CreateIdentificationTypesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/identificationTypes",
     *      summary="Store a newly created IdentificationTypes in storage",
     *      tags={"IdentificationTypes"},
     *      description="Store IdentificationTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IdentificationTypes that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IdentificationTypes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IdentificationTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateIdentificationTypesAPIRequest $request)
    {
        $input = $request->all();

        $identificationTypes = $this->identificationTypesRepository->create($input);

        return $this->sendResponse($identificationTypes->toArray(), 'Identification Types saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/identificationTypes/{id}",
     *      summary="Display the specified IdentificationTypes",
     *      tags={"IdentificationTypes"},
     *      description="Get IdentificationTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IdentificationTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IdentificationTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var IdentificationTypes $identificationTypes */
        $identificationTypes = $this->identificationTypesRepository->find($id);

        if (empty($identificationTypes)) {
            return $this->sendError('Identification Types not found');
        }

        return $this->sendResponse($identificationTypes->toArray(), 'Identification Types retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateIdentificationTypesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/identificationTypes/{id}",
     *      summary="Update the specified IdentificationTypes in storage",
     *      tags={"IdentificationTypes"},
     *      description="Update IdentificationTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IdentificationTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IdentificationTypes that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IdentificationTypes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IdentificationTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateIdentificationTypesAPIRequest $request)
    {
        $input = $request->all();

        /** @var IdentificationTypes $identificationTypes */
        $identificationTypes = $this->identificationTypesRepository->find($id);

        if (empty($identificationTypes)) {
            return $this->sendError('Identification Types not found');
        }

        $identificationTypes = $this->identificationTypesRepository->update($input, $id);

        return $this->sendResponse($identificationTypes->toArray(), 'IdentificationTypes updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/identificationTypes/{id}",
     *      summary="Remove the specified IdentificationTypes from storage",
     *      tags={"IdentificationTypes"},
     *      description="Delete IdentificationTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IdentificationTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var IdentificationTypes $identificationTypes */
        $identificationTypes = $this->identificationTypesRepository->find($id);

        if (empty($identificationTypes)) {
            return $this->sendError('Identification Types not found');
        }

        $identificationTypes->delete();

        return $this->sendSuccess('Identification Types deleted successfully');
    }
}
