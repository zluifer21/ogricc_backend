<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMaterialAPIRequest;
use App\Http\Requests\API\UpdateMaterialAPIRequest;
use App\Models\Material;
use App\Repositories\MaterialRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MaterialController
 * @package App\Http\Controllers\API
 */

class MaterialAPIController extends AppBaseController
{
    /** @var  MaterialRepository */
    private $materialRepository;

    public function __construct(MaterialRepository $materialRepo)
    {
        $this->materialRepository = $materialRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/materials",
     *      summary="Get a listing of the Materials.",
     *      tags={"Material"},
     *      description="Get all Materials",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Material")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $materials = $this->materialRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($materials->toArray(), 'Materials retrieved successfully');
    }

    /**
     * @param CreateMaterialAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/materials",
     *      summary="Store a newly created Material in storage",
     *      tags={"Material"},
     *      description="Store Material",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Material that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Material")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Material"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMaterialAPIRequest $request)
    {
        $input = $request->all();

        $material = $this->materialRepository->create($input);

        return $this->sendResponse($material->toArray(), 'Material saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/materials/{id}",
     *      summary="Display the specified Material",
     *      tags={"Material"},
     *      description="Get Material",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Material",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Material"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Material $material */
        $material = $this->materialRepository->find($id);

        if (empty($material)) {
            return $this->sendError('Material not found');
        }

        return $this->sendResponse($material->toArray(), 'Material retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMaterialAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/materials/{id}",
     *      summary="Update the specified Material in storage",
     *      tags={"Material"},
     *      description="Update Material",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Material",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Material that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Material")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Material"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMaterialAPIRequest $request)
    {
        $input = $request->all();

        /** @var Material $material */
        $material = $this->materialRepository->find($id);

        if (empty($material)) {
            return $this->sendError('Material not found');
        }

        $material = $this->materialRepository->update($input, $id);

        return $this->sendResponse($material->toArray(), 'Material updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/materials/{id}",
     *      summary="Remove the specified Material from storage",
     *      tags={"Material"},
     *      description="Delete Material",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Material",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Material $material */
        $material = $this->materialRepository->find($id);

        if (empty($material)) {
            return $this->sendError('Material not found');
        }

        $material->delete();

        return $this->sendSuccess('Material deleted successfully');
    }
}
