<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMediaTypeAPIRequest;
use App\Http\Requests\API\UpdateMediaTypeAPIRequest;
use App\Models\MediaType;
use App\Repositories\MediaTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MediaTypeController
 * @package App\Http\Controllers\API
 */

class MediaTypeAPIController extends AppBaseController
{
    /** @var  MediaTypeRepository */
    private $mediaTypeRepository;

    public function __construct(MediaTypeRepository $mediaTypeRepo)
    {
        $this->mediaTypeRepository = $mediaTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/mediaTypes",
     *      summary="Get a listing of the MediaTypes.",
     *      tags={"MediaType"},
     *      description="Get all MediaTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MediaType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $mediaTypes = $this->mediaTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($mediaTypes->toArray(), 'Media Types retrieved successfully');
    }

    /**
     * @param CreateMediaTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/mediaTypes",
     *      summary="Store a newly created MediaType in storage",
     *      tags={"MediaType"},
     *      description="Store MediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MediaType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MediaType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MediaType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMediaTypeAPIRequest $request)
    {
        $input = $request->all();

        $mediaType = $this->mediaTypeRepository->create($input);

        return $this->sendResponse($mediaType->toArray(), 'Media Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/mediaTypes/{id}",
     *      summary="Display the specified MediaType",
     *      tags={"MediaType"},
     *      description="Get MediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MediaType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MediaType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MediaType $mediaType */
        $mediaType = $this->mediaTypeRepository->find($id);

        if (empty($mediaType)) {
            return $this->sendError('Media Type not found');
        }

        return $this->sendResponse($mediaType->toArray(), 'Media Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMediaTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/mediaTypes/{id}",
     *      summary="Update the specified MediaType in storage",
     *      tags={"MediaType"},
     *      description="Update MediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MediaType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MediaType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MediaType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MediaType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMediaTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var MediaType $mediaType */
        $mediaType = $this->mediaTypeRepository->find($id);

        if (empty($mediaType)) {
            return $this->sendError('Media Type not found');
        }

        $mediaType = $this->mediaTypeRepository->update($input, $id);

        return $this->sendResponse($mediaType->toArray(), 'MediaType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/mediaTypes/{id}",
     *      summary="Remove the specified MediaType from storage",
     *      tags={"MediaType"},
     *      description="Delete MediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MediaType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MediaType $mediaType */
        $mediaType = $this->mediaTypeRepository->find($id);

        if (empty($mediaType)) {
            return $this->sendError('Media Type not found');
        }

        $mediaType->delete();

        return $this->sendSuccess('Media Type deleted successfully');
    }
}
