<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNeighborhoodAPIRequest;
use App\Http\Requests\API\UpdateNeighborhoodAPIRequest;
use App\Models\Neighborhood;
use App\Repositories\NeighborhoodRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class NeighborhoodController
 * @package App\Http\Controllers\API
 */

class NeighborhoodAPIController extends AppBaseController
{
    /** @var  NeighborhoodRepository */
    private $neighborhoodRepository;

    public function __construct(NeighborhoodRepository $neighborhoodRepo)
    {
        $this->neighborhoodRepository = $neighborhoodRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/neighborhoods",
     *      summary="Get a listing of the Neighborhoods.",
     *      tags={"Neighborhood"},
     *      description="Get all Neighborhoods",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Neighborhood")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $neighborhoods = $this->neighborhoodRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($neighborhoods->toArray(), 'Neighborhoods retrieved successfully');
    }

    /**
     * @param CreateNeighborhoodAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/neighborhoods",
     *      summary="Store a newly created Neighborhood in storage",
     *      tags={"Neighborhood"},
     *      description="Store Neighborhood",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Neighborhood that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Neighborhood")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Neighborhood"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateNeighborhoodAPIRequest $request)
    {
        $input = $request->all();

        $neighborhood = $this->neighborhoodRepository->create($input);

        return $this->sendResponse($neighborhood->toArray(), 'Neighborhood saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/neighborhoods/{id}",
     *      summary="Display the specified Neighborhood",
     *      tags={"Neighborhood"},
     *      description="Get Neighborhood",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Neighborhood",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Neighborhood"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Neighborhood $neighborhood */
        $neighborhood = $this->neighborhoodRepository->find($id);

        if (empty($neighborhood)) {
            return $this->sendError('Neighborhood not found');
        }

        return $this->sendResponse($neighborhood->toArray(), 'Neighborhood retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateNeighborhoodAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/neighborhoods/{id}",
     *      summary="Update the specified Neighborhood in storage",
     *      tags={"Neighborhood"},
     *      description="Update Neighborhood",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Neighborhood",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Neighborhood that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Neighborhood")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Neighborhood"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateNeighborhoodAPIRequest $request)
    {
        $input = $request->all();

        /** @var Neighborhood $neighborhood */
        $neighborhood = $this->neighborhoodRepository->find($id);

        if (empty($neighborhood)) {
            return $this->sendError('Neighborhood not found');
        }

        $neighborhood = $this->neighborhoodRepository->update($input, $id);

        return $this->sendResponse($neighborhood->toArray(), 'Neighborhood updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/neighborhoods/{id}",
     *      summary="Remove the specified Neighborhood from storage",
     *      tags={"Neighborhood"},
     *      description="Delete Neighborhood",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Neighborhood",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Neighborhood $neighborhood */
        $neighborhood = $this->neighborhoodRepository->find($id);

        if (empty($neighborhood)) {
            return $this->sendError('Neighborhood not found');
        }

        $neighborhood->delete();

        return $this->sendSuccess('Neighborhood deleted successfully');
    }


    public function filter(Request $request){
        $neighborhoods =$this->neighborhoodRepository->filter($request->location_id);
        return $this->sendResponse($neighborhoods->toArray(),'Success');
    }
}
