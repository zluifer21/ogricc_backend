<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePersonInformationAPIRequest;
use App\Http\Requests\API\UpdatePersonInformationAPIRequest;
use App\Models\PersonInformation;
use App\Repositories\PersonInformationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PersonInformationController
 * @package App\Http\Controllers\API
 */

class PersonInformationAPIController extends AppBaseController
{
    /** @var  PersonInformationRepository */
    private $personInformationRepository;

    public function __construct(PersonInformationRepository $personInformationRepo)
    {
        $this->personInformationRepository = $personInformationRepo;
    }

    /**
     * Display a listing of the PersonInformation.
     * GET|HEAD /personInformations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $personInformations = $this->personInformationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($personInformations->toArray(), 'Person Informations retrieved successfully');
    }

    /**
     * Store a newly created PersonInformation in storage.
     * POST /personInformations
     *
     * @param CreatePersonInformationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonInformationAPIRequest $request)
    {
        $input = $request->all();

        $personInformation = $this->personInformationRepository->create($input);

        return $this->sendResponse($personInformation->toArray(), 'Person Information saved successfully');
    }

    /**
     * Display the specified PersonInformation.
     * GET|HEAD /personInformations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PersonInformation $personInformation */
        $personInformation = $this->personInformationRepository->find($id);

        if (empty($personInformation)) {
            return $this->sendError('Person Information not found');
        }

        return $this->sendResponse($personInformation->toArray(), 'Person Information retrieved successfully');
    }

    /**
     * Update the specified PersonInformation in storage.
     * PUT/PATCH /personInformations/{id}
     *
     * @param int $id
     * @param UpdatePersonInformationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonInformationAPIRequest $request)
    {
        $input = $request->all();

        /** @var PersonInformation $personInformation */
        $personInformation = $this->personInformationRepository->find($id);

        if (empty($personInformation)) {
            return $this->sendError('Person Information not found');
        }

        $personInformation = $this->personInformationRepository->update($input, $id);

        return $this->sendResponse($personInformation->toArray(), 'PersonInformation updated successfully');
    }

    /**
     * Remove the specified PersonInformation from storage.
     * DELETE /personInformations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PersonInformation $personInformation */
        $personInformation = $this->personInformationRepository->find($id);

        if (empty($personInformation)) {
            return $this->sendError('Person Information not found');
        }

        $personInformation->delete();

        return $this->sendSuccess('Person Information deleted successfully');
    }
}
