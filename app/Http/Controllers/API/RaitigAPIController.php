<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRaitigAPIRequest;
use App\Http\Requests\API\UpdateRaitigAPIRequest;
use App\Models\Raitig;
use App\Repositories\RaitigRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RaitigController
 * @package App\Http\Controllers\API
 */

class RaitigAPIController extends AppBaseController
{
    /** @var  RaitigRepository */
    private $raitigRepository;

    public function __construct(RaitigRepository $raitigRepo)
    {
        $this->raitigRepository = $raitigRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/raitigs",
     *      summary="Get a listing of the Raitigs.",
     *      tags={"Raitig"},
     *      description="Get all Raitigs",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Raitig")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $raitigs = $this->raitigRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($raitigs->toArray(), 'Raitigs retrieved successfully');
    }

    /**
     * @param CreateRaitigAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/raitigs",
     *      summary="Store a newly created Raitig in storage",
     *      tags={"Raitig"},
     *      description="Store Raitig",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Raitig that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Raitig")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Raitig"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRaitigAPIRequest $request)
    {
        $input = $request->all();

        $raitig = $this->raitigRepository->create($input);

        return $this->sendResponse($raitig->toArray(), 'Raitig saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/raitigs/{id}",
     *      summary="Display the specified Raitig",
     *      tags={"Raitig"},
     *      description="Get Raitig",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Raitig",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Raitig"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Raitig $raitig */
        $raitig = $this->raitigRepository->find($id);

        if (empty($raitig)) {
            return $this->sendError('Raitig not found');
        }

        return $this->sendResponse($raitig->toArray(), 'Raitig retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRaitigAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/raitigs/{id}",
     *      summary="Update the specified Raitig in storage",
     *      tags={"Raitig"},
     *      description="Update Raitig",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Raitig",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Raitig that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Raitig")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Raitig"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRaitigAPIRequest $request)
    {
        $input = $request->all();

        /** @var Raitig $raitig */
        $raitig = $this->raitigRepository->find($id);

        if (empty($raitig)) {
            return $this->sendError('Raitig not found');
        }

        $raitig = $this->raitigRepository->update($input, $id);

        return $this->sendResponse($raitig->toArray(), 'Raitig updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/raitigs/{id}",
     *      summary="Remove the specified Raitig from storage",
     *      tags={"Raitig"},
     *      description="Delete Raitig",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Raitig",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Raitig $raitig */
        $raitig = $this->raitigRepository->find($id);

        if (empty($raitig)) {
            return $this->sendError('Raitig not found');
        }

        $raitig->delete();

        return $this->sendSuccess('Raitig deleted successfully');
    }
}
