<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRatingTypeAPIRequest;
use App\Http\Requests\API\UpdateRatingTypeAPIRequest;
use App\Models\RatingType;
use App\Repositories\RatingTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RatingTypeController
 * @package App\Http\Controllers\API
 */

class RatingTypeAPIController extends AppBaseController
{
    /** @var  RatingTypeRepository */
    private $ratingTypeRepository;

    public function __construct(RatingTypeRepository $ratingTypeRepo)
    {
        $this->ratingTypeRepository = $ratingTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ratingTypes",
     *      summary="Get a listing of the RatingTypes.",
     *      tags={"RatingType"},
     *      description="Get all RatingTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/RatingType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ratingTypes = $this->ratingTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ratingTypes->toArray(), 'Rating Types retrieved successfully');
    }

    /**
     * @param CreateRatingTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ratingTypes",
     *      summary="Store a newly created RatingType in storage",
     *      tags={"RatingType"},
     *      description="Store RatingType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RatingType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RatingType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RatingType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRatingTypeAPIRequest $request)
    {
        $input = $request->all();

        $ratingType = $this->ratingTypeRepository->create($input);

        return $this->sendResponse($ratingType->toArray(), 'Rating Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ratingTypes/{id}",
     *      summary="Display the specified RatingType",
     *      tags={"RatingType"},
     *      description="Get RatingType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RatingType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RatingType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var RatingType $ratingType */
        $ratingType = $this->ratingTypeRepository->find($id);

        if (empty($ratingType)) {
            return $this->sendError('Rating Type not found');
        }

        return $this->sendResponse($ratingType->toArray(), 'Rating Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRatingTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ratingTypes/{id}",
     *      summary="Update the specified RatingType in storage",
     *      tags={"RatingType"},
     *      description="Update RatingType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RatingType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RatingType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RatingType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RatingType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRatingTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var RatingType $ratingType */
        $ratingType = $this->ratingTypeRepository->find($id);

        if (empty($ratingType)) {
            return $this->sendError('Rating Type not found');
        }

        $ratingType = $this->ratingTypeRepository->update($input, $id);

        return $this->sendResponse($ratingType->toArray(), 'RatingType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ratingTypes/{id}",
     *      summary="Remove the specified RatingType from storage",
     *      tags={"RatingType"},
     *      description="Delete RatingType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RatingType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var RatingType $ratingType */
        $ratingType = $this->ratingTypeRepository->find($id);

        if (empty($ratingType)) {
            return $this->sendError('Rating Type not found');
        }

        $ratingType->delete();

        return $this->sendSuccess('Rating Type deleted successfully');
    }
}
