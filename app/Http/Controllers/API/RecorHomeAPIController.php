<?php

namespace App\Http\Controllers\API;

use App\Exports\RecordHomeExport;
use App\Http\Requests\API\CreateRecorHomeAPIRequest;
use App\Http\Requests\API\UpdateRecorHomeAPIRequest;
use App\Models\RecorHome;
use App\Repositories\RecorHomeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Response;

/**
 * Class RecorHomeController
 * @package App\Http\Controllers\API
 */

class RecorHomeAPIController extends AppBaseController
{
    /** @var  RecorHomeRepository */
    private $recorHomeRepository;

    public function __construct(RecorHomeRepository $recorHomeRepo)
    {
        $this->recorHomeRepository = $recorHomeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/recorHomes",
     *      summary="Get a listing of the RecorHomes.",
     *      tags={"RecorHome"},
     *      description="Get all RecorHomes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/RecorHome")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $recorHomes = $this->recorHomeRepository->index($request);

        return $this->sendResponse($recorHomes->toArray(), 'Recor Homes retrieved successfully');
    }

    /**
     * @param CreateRecorHomeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/recorHomes",
     *      summary="Store a newly created RecorHome in storage",
     *      tags={"RecorHome"},
     *      description="Store RecorHome",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RecorHome that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RecorHome")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RecorHome"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRecorHomeAPIRequest $request)
    {
        $input = $request->all();

        $input['helps'] = array_map(function($value) {
            return intval($value);
        }, $input['helps']);
        $input['services'] = array_map(function($value) {
            return intval($value);
        }, $input['services']);
        $input['affectation_type'] = array_map(function($value) {
            return intval($value);
        }, $input['affectation_type']);
        $recorHome = $this->recorHomeRepository->store($input);
        return $this->sendResponse($recorHome->toArray(), 'Recor Home saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/recorHomes/{id}",
     *      summary="Display the specified RecorHome",
     *      tags={"RecorHome"},
     *      description="Get RecorHome",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RecorHome",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RecorHome"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var RecorHome $recorHome */
        $recorHome = $this->recorHomeRepository->show($id);

        if (empty($recorHome)) {
            return $this->sendError('Recor Home not found');
        }

        return $recorHome;
    }

    /**
     * @param int $id
     * @param UpdateRecorHomeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/recorHomes/{id}",
     *      summary="Update the specified RecorHome in storage",
     *      tags={"RecorHome"},
     *      description="Update RecorHome",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RecorHome",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RecorHome that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RecorHome")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RecorHome"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRecorHomeAPIRequest $request)
    {
        $input = $request->all();

        /** @var RecorHome $recorHome */
        $recorHome = $this->recorHomeRepository->find($id);

        if (empty($recorHome)) {
            return $this->sendError('Recor Home not found');
        }

        $recorHome = $this->recorHomeRepository->update($input, $id);
        //return $request->all();
        return $this->sendResponse($recorHome, 'RecorHome updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/recorHomes/{id}",
     *      summary="Remove the specified RecorHome from storage",
     *      tags={"RecorHome"},
     *      description="Delete RecorHome",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RecorHome",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var RecorHome $recorHome */
        $recorHome = $this->recorHomeRepository->find($id);

        if (empty($recorHome)) {
            return $this->sendError('Recor Home not found');
        }

        $recorHome->delete();

        return $this->sendSuccess('Recor Home deleted successfully');
    }

    public function init()
    {
        $response = $this->recorHomeRepository->init();
        return $this->sendResponse($response,'');
    }

    public function clone(Request $request){
        $response = $this->recorHomeRepository->clone($request->id);
        return $this->sendResponse($response,'');
    }

    public function export(){
        $filename='excels/'.Str::uuid().'-prices.xlsx';
        $path = Excel::store(new RecordHomeExport() ,$filename, 's3', null, [
            'visibility' => 'public',
        ]);
        if ($path){
            return response()->json([
                'url' => $path = Storage::disk('s3')->url( $filename)
            ]);
        }
        else{
            return response()->json([
                'message' => 'Object cannot be deleted'
            ]);
        }
    }

    public function chart(){
        $response = $this->recorHomeRepository->chart();
        return $this->sendResponse($response,'');
    }
}
