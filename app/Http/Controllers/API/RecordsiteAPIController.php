<?php

namespace App\Http\Controllers\API;

use App\Exports\RecordSiteExport;
use App\Http\Requests\API\CreateRecordsiteAPIRequest;
use App\Http\Requests\API\UpdateRecordsiteAPIRequest;
use App\Models\Recordsite;
use App\Repositories\RecordsiteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Response;

/**
 * Class RecordsiteController
 * @package App\Http\Controllers\API
 */

class RecordsiteAPIController extends AppBaseController
{
    /** @var  RecordsiteRepository */
    private $recordsiteRepository;

    public function __construct(RecordsiteRepository $recordsiteRepo)
    {
        $this->recordsiteRepository = $recordsiteRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/recordsites",
     *      summary="Get a listing of the Recordsites.",
     *      tags={"Recordsite"},
     *      description="Get all Recordsites",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Recordsite")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $recordsites = $this->recordsiteRepository->index(
        $request->all()
        );

        return $this->sendResponse($recordsites->toArray(), 'Recordsites retrieved successfully');
    }

    /**
     * @param CreateRecordsiteAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/recordsites",
     *      summary="Store a newly created Recordsite in storage",
     *      tags={"Recordsite"},
     *      description="Store Recordsite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Recordsite that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Recordsite")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Recordsite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRecordsiteAPIRequest $request)
    {
            $request->request->add(['user_id'=>Auth::user()->id]);
            $input = $request->all();
            $recordsite = $this->recordsiteRepository->store($input);

        return $this->sendResponse($recordsite, 'Registro Almacenado Exitosamente');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/recordsites/{id}",
     *      summary="Display the specified Recordsite",
     *      tags={"Recordsite"},
     *      description="Get Recordsite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Recordsite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Recordsite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Recordsite $recordsite */
        $recordsite = $this->recordsiteRepository->show($id);

        if (empty($recordsite)) {
            return $this->sendError('Recordsite not found');
        }


        return $recordsite;
    }

    /**
     * @param int $id
     * @param UpdateRecordsiteAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/recordsites/{id}",
     *      summary="Update the specified Recordsite in storage",
     *      tags={"Recordsite"},
     *      description="Update Recordsite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Recordsite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Recordsite that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Recordsite")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Recordsite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRecordsiteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Recordsite $recordsite */
        $recordsite = $this->recordsiteRepository->find($id);

        if (empty($recordsite)) {
            return $this->sendError('Recordsite not found');
        }

        $recordsite = $this->recordsiteRepository->update($input, $id);

        return $this->sendResponse($recordsite->toArray(), 'Recordsite updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/recordsites/{id}",
     *      summary="Remove the specified Recordsite from storage",
     *      tags={"Recordsite"},
     *      description="Delete Recordsite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Recordsite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Recordsite $recordsite */
        $recordsite = $this->recordsiteRepository->find($id);

        if (empty($recordsite)) {
            return $this->sendError('Registro no entontrado');
        }

        $recordsite->delete();

        return $this->sendSuccess('Eliminado exitosamente');
    }

    public function init(){
        $response = $this->recordsiteRepository->init();
        return $this->sendResponse($response,'');
    }
    public function clone(Request $request){
        $response = $this->recordsiteRepository->clone($request->id);
        return $this->sendResponse($response,'');
    }

    public function export(){
        $filename='excels/'.Str::uuid().'-prices.xlsx';
        $path = Excel::store(new RecordSiteExport() ,$filename, 's3', null, [
            'visibility' => 'public',
        ]);
        if ($path){
            return response()->json([
                'url' => $path = Storage::disk('s3')->url( $filename)
            ]);
        }
        else{
            return response()->json([
                'message' => 'Object cannot be deleted'
            ]);
        }
    }
}
