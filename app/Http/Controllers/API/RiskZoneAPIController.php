<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRiskZoneAPIRequest;
use App\Http\Requests\API\UpdateRiskZoneAPIRequest;
use App\Models\RiskZone;
use App\Repositories\RiskZoneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RiskZoneController
 * @package App\Http\Controllers\API
 */

class RiskZoneAPIController extends AppBaseController
{
    /** @var  RiskZoneRepository */
    private $riskZoneRepository;

    public function __construct(RiskZoneRepository $riskZoneRepo)
    {
        $this->riskZoneRepository = $riskZoneRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/riskZones",
     *      summary="Get a listing of the RiskZones.",
     *      tags={"RiskZone"},
     *      description="Get all RiskZones",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/RiskZone")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $riskZones = $this->riskZoneRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($riskZones->toArray(), 'Risk Zones retrieved successfully');
    }

    /**
     * @param CreateRiskZoneAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/riskZones",
     *      summary="Store a newly created RiskZone in storage",
     *      tags={"RiskZone"},
     *      description="Store RiskZone",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RiskZone that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RiskZone")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiskZone"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRiskZoneAPIRequest $request)
    {
        $input = $request->all();

        $riskZone = $this->riskZoneRepository->create($input);

        return $this->sendResponse($riskZone->toArray(), 'Risk Zone saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/riskZones/{id}",
     *      summary="Display the specified RiskZone",
     *      tags={"RiskZone"},
     *      description="Get RiskZone",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RiskZone",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiskZone"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var RiskZone $riskZone */
        $riskZone = $this->riskZoneRepository->find($id);

        if (empty($riskZone)) {
            return $this->sendError('Risk Zone not found');
        }

        return $this->sendResponse($riskZone->toArray(), 'Risk Zone retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRiskZoneAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/riskZones/{id}",
     *      summary="Update the specified RiskZone in storage",
     *      tags={"RiskZone"},
     *      description="Update RiskZone",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RiskZone",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RiskZone that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RiskZone")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiskZone"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRiskZoneAPIRequest $request)
    {
        $input = $request->all();

        /** @var RiskZone $riskZone */
        $riskZone = $this->riskZoneRepository->find($id);

        if (empty($riskZone)) {
            return $this->sendError('Risk Zone not found');
        }

        $riskZone = $this->riskZoneRepository->update($input, $id);

        return $this->sendResponse($riskZone->toArray(), 'RiskZone updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/riskZones/{id}",
     *      summary="Remove the specified RiskZone from storage",
     *      tags={"RiskZone"},
     *      description="Delete RiskZone",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RiskZone",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var RiskZone $riskZone */
        $riskZone = $this->riskZoneRepository->find($id);

        if (empty($riskZone)) {
            return $this->sendError('Risk Zone not found');
        }

        $riskZone->delete();

        return $this->sendSuccess('Risk Zone deleted successfully');
    }
}
