<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use Response;


class RolesController extends AppBaseController
{
    public function index()
    {
        $roles= \Spatie\Permission\Models\Role::all();
        return $this->sendResponse($roles, 'Dashboard');
    }
}
