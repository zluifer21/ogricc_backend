<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUnityTypeAPIRequest;
use App\Http\Requests\API\UpdateUnityTypeAPIRequest;
use App\Models\UnityType;
use App\Repositories\UnityTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class UnityTypeController
 * @package App\Http\Controllers\API
 */

class UnityTypeAPIController extends AppBaseController
{
    /** @var  UnityTypeRepository */
    private $unityTypeRepository;

    public function __construct(UnityTypeRepository $unityTypeRepo)
    {
        $this->unityTypeRepository = $unityTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/unityTypes",
     *      summary="Get a listing of the UnityTypes.",
     *      tags={"UnityType"},
     *      description="Get all UnityTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UnityType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $unityTypes = $this->unityTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($unityTypes->toArray(), 'Unity Types retrieved successfully');
    }

    /**
     * @param CreateUnityTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/unityTypes",
     *      summary="Store a newly created UnityType in storage",
     *      tags={"UnityType"},
     *      description="Store UnityType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UnityType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UnityType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UnityType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUnityTypeAPIRequest $request)
    {
        $input = $request->all();

        $unityType = $this->unityTypeRepository->create($input);

        return $this->sendResponse($unityType->toArray(), 'Unity Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/unityTypes/{id}",
     *      summary="Display the specified UnityType",
     *      tags={"UnityType"},
     *      description="Get UnityType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UnityType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UnityType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UnityType $unityType */
        $unityType = $this->unityTypeRepository->find($id);

        if (empty($unityType)) {
            return $this->sendError('Unity Type not found');
        }

        return $this->sendResponse($unityType->toArray(), 'Unity Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUnityTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/unityTypes/{id}",
     *      summary="Update the specified UnityType in storage",
     *      tags={"UnityType"},
     *      description="Update UnityType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UnityType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UnityType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UnityType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UnityType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUnityTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var UnityType $unityType */
        $unityType = $this->unityTypeRepository->find($id);

        if (empty($unityType)) {
            return $this->sendError('Unity Type not found');
        }

        $unityType = $this->unityTypeRepository->update($input, $id);

        return $this->sendResponse($unityType->toArray(), 'UnityType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/unityTypes/{id}",
     *      summary="Remove the specified UnityType from storage",
     *      tags={"UnityType"},
     *      description="Delete UnityType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UnityType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var UnityType $unityType */
        $unityType = $this->unityTypeRepository->find($id);

        if (empty($unityType)) {
            return $this->sendError('Unity Type not found');
        }

        $unityType->delete();

        return $this->sendSuccess('Unity Type deleted successfully');
    }
}
