<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Person;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users= User::with(['roles','areas'])->get();
        return $this->sendResponse($users, 'Dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = New User;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->dni);
        $user->save();
        $role = Role::where('id',$request->role_id)->first();
        $user->assignRole($role);
        $user->areas()->attach($request->area_id);
        $person = new Person;
        $person->name=$request->name;
        $person->lastname=$request->lastname;
        $person->dni=$request->dni;
        $person->neighborhood_id=$request->neighborhood_id;
        $person->indentification_type_id=$request->indentification_type_id;
        $user->persons()->save($person);
        return $this->sendResponse($user, 'Dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
        return $this->sendSuccess('Area deleted successfully');
    }

    public function me()
    {
        $user = Auth::user();
        $user->load('persons','roles');
        $roles = $user->getRoleNames();
        $data=[];
        $data['user']=$user;
        $data['roles']=$roles;
        return $this->sendResponse($data,'Area deleted successfully');
    }
}
