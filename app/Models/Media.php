<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Media",
 *      required={""},
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="medias_type_id",
 *          description="medias_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="entity_id",
 *          description="entity_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="entity_type",
 *          description="entity_type",
 *          type="string"
 *      )
 * )
 */
class Media extends Model
{
    use SoftDeletes;

    public $table = 'medias';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'url',
        'media_types_id',
        'entity_id',
        'entity_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'url' => 'string',
        'id' => 'integer',
        'media_types_id' => 'integer',
        'entity_id' => 'integer',
        'entity_type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mediaType()
    {
        return $this->belongsTo(\App\Models\MediaType::class);
    }

    public function entity()
    {
        return $this->morphTo();
    }
}
