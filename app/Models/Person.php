<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

/**
 * @SWG\Definition(
 *      definition="Person",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="birth_date",
 *          description="birth_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="dni",
 *          description="dni",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="profile_pic",
 *          description="profile_pic",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="neighborhood_id",
 *          description="neighborhood_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="indentification_type_id",
 *          description="indentification_type_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Person extends Model
{
    use SoftDeletes, HasRoles;
    public $table = 'persons';

    protected $guard_name = 'web';
    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'lastname',
        'birth_date',
        'dni',
        'profile_pic',
        'neighborhood_id',
        'user_id',
        'indentification_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function neighborhood()
    {
        return $this->belongsTo(\App\Models\Neighborhood::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function identificationType()
    {
        return $this->belongsTo(\App\Models\IdentificationType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function metas()
    {
        return $this->hasMany(\App\Models\PersonInformation::class);
    }

    public function areas()
    {
        return $this->belongsToMany(\App\Models\Area::class, 'area_person');

    }
}
