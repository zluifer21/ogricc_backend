<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class PersonInformation
 * @package App\Models
 * @version June 18, 2020, 6:38 am UTC
 *
 * @property integer $person_id
 * @property integer $information_id
 * @property string $value
 */
class PersonInformation extends Model
{

    public $table = 'person_information';




    public $fillable = [
        'person_id',
        'information_id',
        'value',
        'key'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'person_id' => 'integer',
        'information_id' => 'integer',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
