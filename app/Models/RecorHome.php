<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @SWG\Definition(
 *      definition="RecorHome",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="latitud",
 *          description="latitud",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitud",
 *          description="longitud",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="neighborhood_id",
 *          description="neighborhood_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="person_id",
 *          description="person_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date",
 *          description="date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="time",
 *          description="time",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="duration",
 *          description="duration",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_persons_home",
 *          description="n_persons_home",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_families",
 *          description="n_families",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="site_id",
 *          description="site_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="property_type_id",
 *          description="property_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="unity_type_id",
 *          description="unity_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="home_status_id",
 *          description="home_status_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="material_id",
 *          description="material_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="normativity",
 *          description="normativity",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="age",
 *          description="age",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="risks_zone_id",
 *          description="risks_zone_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="floor_type_id",
 *          description="floor_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="services",
 *          description="services",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="stratum",
 *          description="stratum",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="risk",
 *          description="risk",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="affectation_type_id",
 *          description="affectation_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_person_wounds",
 *          description="n_person_wounds",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_person_deads",
 *          description="n_person_deads",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="help_id",
 *          description="help_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class RecorHome extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    use SoftDeletes;

    public $table = 'record_homes';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'latitud',
        'longitud',
        'neighborhood_id',
        'person_id',
        'date',
        'time',
        'duration',
        'n_persons_home',
        'n_families',
        'site_id',
        'property_type_id',
        'unity_type_id',
        'home_status_id',
        'material_id',
        'normativity',
        'age',
        'risks_zone_id',
        'floor_type_id',
        'stratum',
        'risk',
        'description',
        'n_person_wounds',
        'n_person_deads',
        'created_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'latitud' => 'string',
        'longitud' => 'string',
        'neighborhood_id' => 'integer',
        'person_id' => 'integer',
        'duration' => 'integer',
        'n_persons_home' => 'integer',
        'n_families' => 'integer',
        'site_id' => 'integer',
        'property_type_id' => 'integer',
        'unity_type_id' => 'integer',
        'home_status_id' => 'integer',
        'material_id' => 'integer',
        'normativity' => 'string',
        'age' => 'integer',
        'risks_zone_id' => 'integer',
        'floor_type_id' => 'integer',
        'stratum' => 'integer',
        'risk' => 'string',
        'affectation_type_id' => 'integer',
        'n_person_wounds' => 'integer',
        'n_person_deads' => 'integer',
        'help_id' => 'integer'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function help()
    {
        return $this->belongsToMany(\App\Models\Help::class,'help_record_homes');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function affectationType()
    {
        return $this->belongsToMany(\App\Models\AffectationType::class,'affectation_type_record_homes');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function floorType()
    {
        return $this->belongsTo(\App\Models\FloorType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function riskZone()
    {
        return $this->belongsTo(\App\Models\RiskZone::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function material()
    {
        return $this->belongsTo(\App\Models\Material::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unityType()
    {
        return $this->belongsTo(\App\Models\UnityType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function propertyType()
    {
        return $this->belongsTo(\App\Models\PropertyType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function homeStatus()
    {
        return $this->belongsTo(\App\Models\HomeStatus::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function site()
    {
        return $this->belongsTo(\App\Models\Site::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function person()
    {
        return $this->belongsTo(\App\Models\Person::class)->with('metas');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function neighborhood()
    {
        return $this->belongsTo(\App\Models\Neighborhood::class);
    }

    public function medias()
    {
        return $this->morphMany(\App\Models\Media::class, 'entity');
    }
    public function services()
    {
        return $this->belongsToMany(\App\Models\Service::class, 'service_rercord_home');
    }


}
