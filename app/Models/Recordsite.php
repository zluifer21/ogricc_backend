<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @SWG\Definition(
 *      definition="Recordsite",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="latitud",
 *          description="latitud",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitud",
 *          description="longitud",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="neighborhood_id",
 *          description="neighborhood_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date",
 *          description="date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="characteristics_place_id",
 *          description="characteristics_place_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="alert_level_id",
 *          description="alert_level_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="duration",
 *          description="duration",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_families",
 *          description="n_families",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_homes",
 *          description="n_homes",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_schools",
 *          description="n_schools",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_healthcares",
 *          description="n_healthcares",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_person_affecteds",
 *          description="n_person_affecteds",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="risks_zone_id",
 *          description="risks_zone_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="floor_type_id",
 *          description="floor_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="affectation_type_id",
 *          description="affectation_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_person_wounds",
 *          description="n_person_wounds",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="n_person_deads",
 *          description="n_person_deads",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="observations",
 *          description="observations",
 *          type="string"
 *      )
 * )
 */
class Recordsite extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    public $table = 'record_sites';
    protected $dates = ['deleted_at'];
    public $fillable = [
        'latitud',
        'longitud',
        'neighborhood_id',
        'date',
        'characteristics_place_id',
        'alert_level_id',
        'duration',
        'n_families',
        'n_homes',
        'n_schools',
        'n_healthcares',
        'n_person_affecteds',
        'risks_zone_id',
        'floor_type_id',
        'n_person_wounds',
        'n_person_deads',
        'observations',
        'user_id',
        'time',
        'created_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'latitud' => 'string',
        'longitud' => 'string',
        'neighborhood_id' => 'integer',
        'characteristics_place_id' => 'integer',
        'alert_level_id' => 'integer',
        'duration' => 'integer',
        'n_families' => 'integer',
        'n_homes' => 'integer',
        'n_schools' => 'integer',
        'n_healthcares' => 'integer',
        'n_person_affecteds' => 'integer',
        'risks_zone_id' => 'integer',
        'floor_type_id' => 'integer',
        'affectation_type_id' => 'integer',
        'n_person_wounds' => 'integer',
        'n_person_deads' => 'integer',
        'observations' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function neighborhood()
    {
        return $this->belongsTo(\App\Models\Neighborhood::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function characteristicPlace()
    {
        return $this->belongsTo(\App\Models\CharacteristicPlace::class,'characteristics_place_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function alertLevel()
    {
        return $this->belongsTo(\App\Models\AlertLevel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function riskZone()
    {
        return $this->belongsTo(\App\Models\RiskZone::class,'risks_zone_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function floorType()
    {
        return $this->belongsTo(\App\Models\FloorType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function affectationType()
    {
        return $this->belongsToMany(\App\Models\AffectationType::class,'affectation_type_record_sites','recor_site_id','affectation_type_id');
    }

    public function rating()
    {
        return $this->belongsToMany(\App\Models\Raitig::class, 'record_site_raiting');
    }

    public function medias()
    {
        return $this->morphMany(\App\Models\Media::class, 'entity');
    }
}
