<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\RecorHome;
class Service extends Model
{
    use SoftDeletes;

    public $table = 'services';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',

    ];
    public function record_home()
    {
        return $this->belongsToMany(\App\Models\RecorHome::class, 'service_rercord_home');
    }
}
