<?php

namespace App\Repositories;

use App\Models\AffectationType;
use App\Repositories\BaseRepository;

/**
 * Class AffectationTypeRepository
 * @package App\Repositories
 * @version June 18, 2020, 7:27 am UTC
*/

class AffectationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AffectationType::class;
    }
}
