<?php

namespace App\Repositories;

use App\Models\AlertLevel;
use App\Repositories\BaseRepository;

/**
 * Class AlertLevelRepository
 * @package App\Repositories
 * @version June 18, 2020, 7:22 am UTC
*/

class AlertLevelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AlertLevel::class;
    }
}
