<?php

namespace App\Repositories;

use App\Models\Bitacora;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class BitacoraRepository
 * @package App\Repositories
 * @version July 3, 2020, 3:25 am UTC
*/

class BitacoraRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'time',
        'affectation_type_id',
        'user_id',
        'neighborhood_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bitacora::class;
    }

    public function index()
    {
        if(!Auth::user()->hasRole('Director')){
            $bitacoras = $this->model->where('user_id',Auth::user()->id)->get();

        }else{
            $bitacoras=$this->model->all();
        }
        $bitacoras->load(['neighborhood','affectationType']);
        return $bitacoras;
    }

    public function store($data)
    {
        $model = $this->model->newInstance($data);
        $model->user_id=Auth::user()->id;
        $model->save();
        $model->load(['neighborhood','affectationType']);
        return $model;
    }
}
