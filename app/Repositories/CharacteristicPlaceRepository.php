<?php

namespace App\Repositories;

use App\Models\CharacteristicPlace;
use App\Repositories\BaseRepository;

/**
 * Class CharacteristicPlaceRepository
 * @package App\Repositories
 * @version June 18, 2020, 7:20 am UTC
*/

class CharacteristicPlaceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CharacteristicPlace::class;
    }
}
