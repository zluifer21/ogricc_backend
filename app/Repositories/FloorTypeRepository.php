<?php

namespace App\Repositories;

use App\Models\FloorType;
use App\Repositories\BaseRepository;

/**
 * Class FloorTypeRepository
 * @package App\Repositories
 * @version June 18, 2020, 7:26 am UTC
*/

class FloorTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FloorType::class;
    }
}
