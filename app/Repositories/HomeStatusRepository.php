<?php

namespace App\Repositories;

use App\Models\HomeStatus;
use App\Repositories\BaseRepository;

/**
 * Class HomeStatusRepository
 * @package App\Repositories
 * @version July 3, 2020, 4:00 am UTC
*/

class HomeStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HomeStatus::class;
    }
}
