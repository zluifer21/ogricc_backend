<?php

namespace App\Repositories;

use App\Models\IdentificationType;
use App\Repositories\BaseRepository;

/**
 * Class IdentificationTypesRepository
 * @package App\Repositories
 * @version June 18, 2020, 6:20 am UTC
*/

class IdentificationTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IdentificationType::class;
    }
}
