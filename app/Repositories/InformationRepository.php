<?php

namespace App\Repositories;

use App\Models\Information;
use App\Repositories\BaseRepository;

/**
 * Class InformationRepository
 * @package App\Repositories
 * @version June 18, 2020, 6:36 am UTC
*/

class InformationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Information::class;
    }
}
