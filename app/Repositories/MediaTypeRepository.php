<?php

namespace App\Repositories;

use App\Models\MediaType;
use App\Repositories\BaseRepository;

/**
 * Class MediaTypeRepository
 * @package App\Repositories
 * @version June 22, 2020, 11:28 am UTC
*/

class MediaTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MediaType::class;
    }
}
