<?php

namespace App\Repositories;

use App\Models\Neighborhood;
use App\Repositories\BaseRepository;

/**
 * Class NeighborhoodRepository
 * @package App\Repositories
 * @version June 18, 2020, 5:57 am UTC
*/

class NeighborhoodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'location_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Neighborhood::class;
    }

    public function filter($location_id){
        return $this->model::where('location_id',$location_id)->get();
    }
}
