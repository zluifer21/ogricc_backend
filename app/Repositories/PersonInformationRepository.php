<?php

namespace App\Repositories;

use App\Models\PersonInformation;
use App\Repositories\BaseRepository;

/**
 * Class PersonInformationRepository
 * @package App\Repositories
 * @version June 18, 2020, 6:38 am UTC
*/

class PersonInformationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'person_id',
        'information_id',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PersonInformation::class;
    }
}
