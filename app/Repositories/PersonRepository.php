<?php

namespace App\Repositories;

use App\Models\AUX\Metas;
use App\Models\Person;
use App\Models\PersonInformation;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

/**
 * Class PersonRepository
 * @package App\Repositories
 * @version June 18, 2020, 6:31 am UTC
*/

class PersonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'lastname',
        'birth_date',
        'dni',
        'profile_pic',
        'neighborhood_id',
        'user_id',
        'indentification_type_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Person::class;
    }

    public function store($data)
    {
        $model = $this->model->newInstance($data);
        $model->neighborhood_id=Auth::user()->persons->neighborhood_id;
        $model->save();
        $model->areas()->attach(Auth::user()->areas->first()->id);
        foreach ($data['metas'] as $meta){
            $meta_new = New PersonInformation();
            $meta_new->key=$meta['key'];
            $meta_new->value=$meta['value'];
            $meta_new->person_id=$model->id;
            $meta_new->save();
        }
        if($data['role_id']){
            $role= Role::where('id',$data['role_id'])->first();
            $model->assignRole($role);
        }
        return $model;

    }

    public function index($data)
    {
        $user = Auth::user();
        if( Auth::user()->hasRole('Administrador') )
        {
            $query = $this->model->query();
            if ($data['cobasa']) {
                $query->whereHas('roles', function ($q) {
                    $q->where('id', 4);
                });

            }

        }else {

            $query = $this->model->query();
            if ($data['cobasa']) {
                $query->whereHas('roles', function ($q) {
                    $q->where('id', 4);
                });
                $query->where('neighborhood_id', $user->persons->neighborhood_id);
            }
            $areasId = Auth::user()->areas()->pluck('areas.id')->toArray();
            $query->whereHas('areas', function ($q) use ($areasId) {
                $q->whereIn('id', $areasId);
            });


        }
        $persons = $query->get();
            foreach ($persons as $person) {
                $person = $this->simpleFormat($person);
            }

        return $persons;
    }

    public function updateMetas($data,$person){
        $metas=$data['metas'];
        $objectMetas=$person->metas;
        if($metas && is_array($metas)){
            foreach ($metas as $meta) {
                    $personMeta = $objectMetas->where('key', $meta['key'])->first();
                    if ($personMeta) {
                        $personMeta->value = $meta['value'];
                        $personMeta->save();
                    } else {
                        $personMeta = new PersonInformation;
                        $personMeta->key = $meta['key'];
                        $personMeta->value = $meta['value'];
                        $personMeta->person_id = $person->id;
                        $personMeta->save();
                    }
                }
            }
        $person = $this->simpleFormat($person);
        return $person;
    }

}
