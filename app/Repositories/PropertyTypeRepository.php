<?php

namespace App\Repositories;

use App\Models\PropertyType;
use App\Repositories\BaseRepository;

/**
 * Class PropertyTypeRepository
 * @package App\Repositories
 * @version July 3, 2020, 3:52 am UTC
*/

class PropertyTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropertyType::class;
    }
}
