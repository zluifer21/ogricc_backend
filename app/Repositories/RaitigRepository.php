<?php

namespace App\Repositories;

use App\Models\Raitig;
use App\Repositories\BaseRepository;

/**
 * Class RaitigRepository
 * @package App\Repositories
 * @version June 18, 2020, 7:38 am UTC
*/

class RaitigRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'raiting_type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Raitig::class;
    }
}
