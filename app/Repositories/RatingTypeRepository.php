<?php

namespace App\Repositories;

use App\Models\RatingType;
use App\Repositories\BaseRepository;

/**
 * Class RatingTypeRepository
 * @package App\Repositories
 * @version June 18, 2020, 7:35 am UTC
*/

class RatingTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RatingType::class;
    }
}
