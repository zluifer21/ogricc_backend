<?php

namespace App\Repositories;

use App\Models\AffectationType;
use App\Models\AlertLevel;
use App\Models\AUX\Metas;
use App\Models\CharacteristicPlace;
use App\Models\FloorType;
use App\Models\Gender;
use App\Models\Help;
use App\Models\HomeStatus;
use App\Models\IdentificationType;
use App\Models\Material;
use App\Models\Media;
use App\Models\Person;
use App\Models\PersonInformation;
use App\Models\PropertyType;
use App\Models\Raitig;
use App\Models\RecorHome;
use App\Models\RiskZone;
use App\Models\Service;
use App\Models\Site;
use App\Models\UnityType;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

/**
 * Class RecorHomeRepository
 * @package App\Repositories
 * @version July 3, 2020, 4:21 am UTC
*/

class RecorHomeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'latitud',
        'longitud',
        'neighborhood_id',
        'person_id',
        'date',
        'time',
        'duration',
        'n_persons_home',
        'n_families',
        'site_id',
        'property_type_id',
        'unity_type_id',
        'home_status_id',
        'material_id',
        'normativity',
        'age',
        'risks_zone_id',
        'floor_type_id',
        'services',
        'stratum',
        'risk',
        'affectation_type_id',
        'n_person_wounds',
        'n_person_deads',
        'help_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RecorHome::class;
    }

    public function init()
    {
        $response = new class(){};
        $response->genders          =Gender::all();
        $response->riskzones        =RiskZone::all();
        $response->floor_types      =FloorType::all();
        $response->affectation_types=AffectationType::all();
        $response->helps            =Help::all();
        $response->materiales       =Material::all();
        $response->sites            =Site::all();
        $response->property_types   =PropertyType::all();
        $response->unities_types    =UnityType::all();
        $response->home_status      =HomeStatus::all();
        $response->identifiaction_types = IdentificationType::all();
        $response->services = Service::all();
        return $response;

    }
    public function update($input, $id)
    {
        $record = json_decode($input['data']);
        $person =  Person::findOrFail($record->persona->id);
        $person->update((array)$record->persona);
        $object = $this->model::findOrFail($id);
        $record->person_id=$person->id;
        $object->update((array)$record);
        $objectMetas = $person->metas;
        if($record->persona->metas && is_array($record->persona->metas)){
            foreach ($record->persona->metas as $meta){
                if(empty($meta->value)){
                    $personMeta = $objectMetas->where('key', $meta->key)->first();
                    if( $personMeta) $personMeta->delete();
                }else {
                    $personMeta = $objectMetas->where('key', $meta->key)->first();
                    if ($personMeta) {
                        $personMeta->value = $meta->value;
                        $personMeta->save();
                    } else {
                        $meta_new = New PersonInformation();
                        $meta_new->key=$meta->key;
                        $meta_new->value=$meta->value;
                        $meta_new->person_id=$person->id;
                        $meta_new->save();
                    }
                }
            }
        }
        if(isset($input['images'])) {
            foreach ($input['images'] as $image){
                $imagename = date('Y-m-d').'-'.$image->getClientOriginalName();
                $path = Storage::disk('s3')->putFileAs('images/carcontent',$image,$imagename,'public');
                $objectmedia = New Media;
                $objectmedia->url=Storage::disk('s3')->url($path);
                $objectmedia->media_types_id=1;
                $object->medias()->save($objectmedia);
            }
        }
        $object->affectationType()->detach();
        $object->help()->detach();
        $object->services()->detach();
        $object->services()->attach($input['services']);
        $object->help()->attach($input['helps']);
        $object->affectationType()->attach($input['affectation_type']);

        return (array)$record->persona;

    }

    public function index($data)
    {
        $query=$this->model->query();
        if(isset($data['alert_level_id'])){
            $query->where('alert_level_id',$data['alert_level_id']);
        }
        if(isset($data['affectation_type_id'])){
            $query->where('affectation_type_id',$data['affectation_type_id']);
        }
        if(isset($data['date'])){
            $query->where('date',$data['date']);
        }
        $records = $query->orderBy('id','DESC')->get();
        $records->load(['person','affectationType','help','neighborhood']);
        return $records;

    }


    public function store($data)
    {
        $record = json_decode($data['data']);
        $person =  new Person;
        $person->fill((array)$record->persona);
        $person->save();
        $record->person_id=$person->id;
        $model = $this->model->newInstance($record);
        $model->save();
        foreach ($record->persona->metas as $meta)
        {
            $meta_new = New PersonInformation();
            $meta_new->key=$meta->key;
            $meta_new->value=$meta->value;
            $meta_new->person_id=$person->id;
            $meta_new->save();
        }
        $model->services()->attach($data['services']);
        $model->help()->attach($data['helps']);
        $model->affectationType()->attach($data['affectation_type']);

        if(isset($data['images'])) {
        foreach ($data['images'] as $image){
            $imagename = date('Y-m-d').'-'.$image->getClientOriginalName();
            $path = Storage::disk('s3')->putFileAs('images/carcontent',$image,$imagename,'public');
            $objectmedia = New Media;
            $objectmedia->url=Storage::disk('s3')->url($path);
            $objectmedia->media_types_id=1;
            $model->medias()->save($objectmedia);
        }
        }
        return $model;
    }

    public function clone($id)
    {
        $record = RecorHome::where('id',$id)->first();
        $record->load(['services','help','affectationType','medias']);
        $newModel = $record->replicate();
        $newModel->push();
        foreach($record->getRelations() as $relation => $items){
            foreach($items as $item){
                unset($item->id);
                $newModel->{$relation}()->create($item->toArray());
            }
        }
        return $newModel;
    }

    public function show($id){
        $object = $this->model::with('person','neighborhood','medias')->where('id',$id)->first();
        $object->serviceid=$object->services->pluck('id')->toArray();
        $object->affectationTypeid=$object->affectationType->pluck('id')->toArray();
        $object->helpid=$object->help->pluck('id')->toArray();
        $object->person = $this->simpleFormat($object->person);
        unset($object->help,$object->services,$object->help,$object->affectation_type);
        return $object;
    }

    public function chart(){
        $query=$this->model->query();
        $users=$query->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });
        $usermcount = [];
        $userArr = [];

        foreach ($users as $key => $value) {
            $usermcount[(int)$key] = count($value);
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($usermcount[$i])){
                array_push($userArr,$usermcount[$i]);

            }else{
                array_push($userArr,0);
            }
        }
        return $userArr;
    }
}
