<?php

namespace App\Repositories;

use App\Models\AffectationType;
use App\Models\AlertLevel;
use App\Models\AUX\Metas;
use App\Models\CharacteristicPlace;
use App\Models\FloorType;
use App\Models\Media;
use App\Models\Raitig;
use App\Models\Recordsite;
use App\Models\RiskZone;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class RecordsiteRepository
 * @package App\Repositories
 * @version June 22, 2020, 11:48 am UTC
*/

class RecordsiteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'latitud',
        'longitud',
        'neighborhood_id',
        'date',
        'characteristics_place_id',
        'alert_level_id',
        'duration',
        'n_families',
        'n_homes',
        'n_schools',
        'n_healthcares',
        'n_person_affecteds',
        'risks_zone_id',
        'floor_type_id',
        'affectation_type_id',
        'n_person_wounds',
        'n_person_deads',
        'observations'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Recordsite::class;
    }

    public function index($data)
    {
        $query=$this->model->query();
        if(isset($data['alert_level_id'])){
            $query->where('alert_level_id',$data['alert_level_id']);
        }
        if(isset($data['affectation_type_id'])){
            $query->where('affectation_type_id',$data['affectation_type_id']);
        }
        if(isset($data['date'])){
            $query->where('date',$data['date']);
        }
        $records = $query->orderBy('id','DESC')->get();
        $records->load(['affectationType','characteristicPlace','neighborhood','alertLevel']);
        return $records;

    }

    public function init(){
        $response = new class(){};
        $response->places=CharacteristicPlace::all();
        $response->alerts=AlertLevel::all();
        $response->riskzones=RiskZone::all();
        $response->floor_types=FloorType::all();
        $response->affectation_types=AffectationType::all();
        $response->geograficas=Raitig::where('raiting_type',1)->get();
        $response->ambientales=Raitig::where('raiting_type',2)->get();
        $response->sociales=Raitig::where('raiting_type',3)->get();
        $response->emergencias=Raitig::where('raiting_type',4)->get();
        $response->institucionales=Raitig::where('raiting_type',5)->get();
        $response->territoriales=Raitig::where('raiting_type',6)->get();
        return $response;
    }

    public function show($id){
        $object = $this->model::with('neighborhood','medias')->where('id',$id)->first();
        $object->geograficas_id=$object->join('record_site_raiting','record_site_raiting.recordsite_id','record_sites.id')
                            ->join('raitings','raitings.id','record_site_raiting.raitig_id')
                            ->where('raitings.raiting_type',1)
                            ->where('record_sites.id',$object->id)
            ->select('raitings.id')->first();
        $object->affectationTypeid=$object->affectationType->pluck('id')->toArray();
        $object->ambientales_id=$object->join('record_site_raiting','record_site_raiting.recordsite_id','record_sites.id')
            ->join('raitings','raitings.id','record_site_raiting.raitig_id')
            ->where('raitings.raiting_type',2)
            ->where('record_sites.id',$object->id)
            ->select('raitings.id')->first();
        $object->sociales_id=$object->join('record_site_raiting','record_site_raiting.recordsite_id','record_sites.id')
            ->join('raitings','raitings.id','record_site_raiting.raitig_id')
            ->where('raitings.raiting_type',3)
            ->where('record_sites.id',$object->id)
            ->select('raitings.id')->first();
        $object->emergencias_id=$object->join('record_site_raiting','record_site_raiting.recordsite_id','record_sites.id')
            ->join('raitings','raitings.id','record_site_raiting.raitig_id')
            ->where('raitings.raiting_type',4)
            ->where('record_sites.id',$object->id)
            ->select('raitings.id')->first();
        $object->institucionales_id=$object->join('record_site_raiting','record_site_raiting.recordsite_id','record_sites.id')
            ->join('raitings','raitings.id','record_site_raiting.raitig_id')
            ->where('raitings.raiting_type',5)
            ->where('record_sites.id',$object->id)
            ->select('raitings.id')->first();
        $object->territoriales_id=$object->join('record_site_raiting','record_site_raiting.recordsite_id','record_sites.id')
            ->join('raitings','raitings.id','record_site_raiting.raitig_id')
            ->where('raitings.raiting_type',6)
            ->where('record_sites.id',$object->id)
            ->select('raitings.id')->first();
        // unset($object->help,$object->services,$object->help,$object->affectation_type);
        return $object;
    }
    public function store($data)
    {
        $model = $this->model->newInstance($data);
        $model->save();
        $model->rating()->attach($data['ratings']);
        $model->affectationType()->attach($data['affectation_type']);
        if(isset($data['images'])) {
            foreach ($data['images'] as $image) {
                $imagename = date('Y-m-d') . '-' . $image->getClientOriginalName();
                $path = Storage::disk('s3')->putFileAs('images/carcontent', $image, $imagename, 'public');
                $objectmedia = new Media;
                $objectmedia->url = Storage::disk('s3')->url($path);
                $objectmedia->media_types_id = 1;
                $model->medias()->save($objectmedia);
            }
        }
        //$model->load(["medias"]);
        return $model;
    }

    public function update($data,$id)
    {
        $object = $this->model::findOrFail($id);
        $object->update($data);
        $object->affectationType()->detach();
        $object->rating()->detach();
        $object->affectationType()->attach($data['affectation_type']);
        $object->rating()->attach($data['ratings']);
        if(isset($data['images'])) {
            foreach ($data['images'] as $image) {
                $imagename = date('Y-m-d') . '-' . $image->getClientOriginalName();
                $path = Storage::disk('s3')->putFileAs('images/carcontent', $image, $imagename, 'public');
                $objectmedia = new Media;
                $objectmedia->url = Storage::disk('s3')->url($path);
                $objectmedia->media_types_id = 1;
                $object->medias()->save($objectmedia);
            }
        }
        return $object;
    }

    public function clone($id)
    {
        $record = Recordsite::where('id',$id)->first();
        $record->load(['rating','affectationType','medias']);
        $newModel = $record->replicate();
        $newModel->push();
        foreach($record->getRelations() as $relation => $items){
            foreach($items as $item){
                unset($item->id);
                $newModel->{$relation}()->create($item->toArray());
            }
        }
        return $newModel;
    }

    public function chart(){
        $query=$this->model->query();
        $users=$query->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });
        $usermcount = [];
        $userArr = [];

        foreach ($users as $key => $value) {
            $usermcount[(int)$key] = count($value);
        }

        /*for($i = 1; $i <= 12; $i++){
            if(!empty($usermcount[$i])){
                $userArr[$i] = $usermcount[$i];
            }else{
                $userArr[$i] = 0;
            }
        }*/
        for($i = 1; $i <= 12; $i++){
            if(!empty($usermcount[$i])){
                array_push($userArr,$usermcount[$i]);

            }else{
                array_push($userArr,0);
            }
        }
        return $userArr;
    }
}
