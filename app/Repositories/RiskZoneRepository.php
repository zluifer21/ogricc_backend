<?php

namespace App\Repositories;

use App\Models\RiskZone;
use App\Repositories\BaseRepository;

/**
 * Class RiskZoneRepository
 * @package App\Repositories
 * @version June 18, 2020, 7:24 am UTC
*/

class RiskZoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RiskZone::class;
    }
}
