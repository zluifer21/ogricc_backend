<?php

namespace App\Repositories;

use App\Models\Site;
use App\Repositories\BaseRepository;

/**
 * Class SiteRepository
 * @package App\Repositories
 * @version July 3, 2020, 3:50 am UTC
*/

class SiteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Site::class;
    }
}
