<?php

namespace App\Repositories;

use App\Models\UnityType;
use App\Repositories\BaseRepository;

/**
 * Class UnityTypeRepository
 * @package App\Repositories
 * @version July 3, 2020, 3:54 am UTC
*/

class UnityTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UnityType::class;
    }
}
