<?php


namespace App\Traits;


trait MetaData
{
    public function simpleFormat(&$object)
    {
        $simple_metas = [];
        foreach ($object->metas as $meta) {
            $simple_metas[$meta->key] = $meta->value;
        }
        $object->unsetRelation('metas');
        $object->metas = $simple_metas;
        return $object;
    }
}
