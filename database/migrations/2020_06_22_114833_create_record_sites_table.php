<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordSitesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('latitud');
            $table->string('longitud');
            $table->integer('neighborhood_id')->unsigned();
            $table->dateTime('date');
            $table->integer('characteristics_place_id')->unsigned();
            $table->integer('alert_level_id')->unsigned();
            $table->integer('duration');
            $table->integer('n_families');
            $table->integer('n_homes');
            $table->integer('n_schools');
            $table->integer('n_healthcares');
            $table->integer('n_person_affecteds');
            $table->integer('risks_zone_id')->unsigned();
            $table->integer('floor_type_id')->unsigned();
            $table->integer('affectation_type_id')->unsigned();
            $table->integer('n_person_wounds');
            $table->integer('n_person_deads');
            $table->text('observations');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('neighborhood_id')->references('id')->on('neighborhoods');
            $table->foreign('characteristics_place_id')->references('id')->on('characteristics_place');
            $table->foreign('alert_level_id')->references('id')->on('alert_levels');
            $table->foreign('risks_zone_id')->references('id')->on('risks_zone');
            $table->foreign('floor_type_id')->references('id')->on('floor_types');
            $table->foreign('affectation_type_id')->references('id')->on('affectation_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('record_sites');
    }
}
