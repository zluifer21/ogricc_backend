<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecordSiteRaiting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_site_raiting', function (Blueprint $table) {
            $table->integer('raiting_id')->unsigned();
            $table->integer('record_site_id')->unsigned();
            $table->foreign('raiting_id')->references('id')->on('raitings')
                ->onDelete('cascade');
            $table->foreign('record_site_id')->references('id')->on('record_sites')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
