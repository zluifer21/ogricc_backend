<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacorasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacoras', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->time('time');
            $table->integer('affectation_type_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->integer('neighborhood_id')->unsigned();
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('affectation_type_id')->references('id')->on('affectation_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('neighborhood_id')->references('id')->on('neighborhoods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bitacoras');
    }
}
