<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordHomesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_homes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('latitud');
            $table->string('longitud');
            $table->integer('neighborhood_id')->unsigned();
            $table->integer('person_id')->unsigned();
            $table->date('date');
            $table->time('time');
            $table->smallInteger('duration');
            $table->smallInteger('n_persons_home');
            $table->smallInteger('n_families');
            $table->integer('site_id')->unsigned();
            $table->integer('property_type_id')->unsigned();
            $table->integer('unity_type_id')->unsigned();
            $table->integer('home_status_id')->unsigned();
            $table->integer('material_id')->unsigned();
            $table->boolean('normativity');
            $table->smallInteger('age');
            $table->integer('risks_zone_id')->unsigned();
            $table->integer('floor_type_id')->unsigned();
            $table->boolean('services');
            $table->smallInteger('stratum');
            $table->boolean('risk');
            $table->integer('affectation_type_id')->unsigned();
            $table->smallInteger('n_person_wounds');
            $table->smallInteger('n_person_deads');
            $table->integer('help_id')->unsigned();
            $table->softDeletes();
            $table->foreign('neighborhood_id')->references('id')->on('neighborhoods');
            $table->foreign('person_id')->references('id')->on('persons');
            $table->foreign('site_id')->references('id')->on('sites');
            $table->foreign('property_type_id')->references('id')->on('property_types');
            $table->foreign('unity_type_id')->references('id')->on('unity_types');
            $table->foreign('home_status_id')->references('id')->on('home_status');
            $table->foreign('material_id')->references('id')->on('materials');
            $table->foreign('risks_zone_id')->references('id')->on('risks_zone');
            $table->foreign('floor_type_id')->references('id')->on('floor_types');
            $table->foreign('affectation_type_id')->references('id')->on('affectation_types');
            $table->foreign('help_id')->references('id')->on('helps');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('record_homes');
    }
}
