<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatPersonAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_person', function (Blueprint $table) {
            $table->integer('person_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons')
                ->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('areas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
