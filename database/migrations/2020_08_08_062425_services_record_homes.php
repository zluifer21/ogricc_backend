<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServicesRecordHomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_rercord_home', function (Blueprint $table) {

            $table->integer('recor_home_id')->unsigned();

            $table->integer('service_id')->unsigned();

            $table->foreign('service_id')->references('id')->on('services')

                ->onDelete('cascade');

            $table->foreign('recor_home_id')->references('id')->on('record_homes')

                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
