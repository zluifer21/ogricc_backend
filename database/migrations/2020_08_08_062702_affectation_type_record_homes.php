<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AffectationTypeRecordHomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affectation_type_record_homes', function (Blueprint $table) {

            $table->integer('recor_home_id')->unsigned();

            $table->integer('affectation_type_id')->unsigned();

            $table->foreign('affectation_type_id')->references('id')->on('affectation_types')
                ->onDelete('cascade');

            $table->foreign('recor_home_id')->references('id')->on('record_homes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
