<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HelpRecordHomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_record_homes', function (Blueprint $table) {

            $table->integer('help_id')->unsigned();

            $table->integer('recor_home_id')->unsigned();

            $table->foreign('help_id')->references('id')->on('helps')
                ->onDelete('cascade');

            $table->foreign('recor_home_id')->references('id')->on('record_homes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
