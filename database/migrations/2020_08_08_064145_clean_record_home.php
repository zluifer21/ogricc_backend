<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CleanRecordHome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('record_homes', function (Blueprint $table) {
            $table->dropForeign('record_homes_affectation_type_id_foreign');
            $table->dropColumn("affectation_type_id");
            $table->dropForeign('record_homes_help_id_foreign');
            $table->dropColumn("help_id");
            $table->dropColumn("services");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_homes', function (Blueprint $table) {
            //
        });
    }
}
