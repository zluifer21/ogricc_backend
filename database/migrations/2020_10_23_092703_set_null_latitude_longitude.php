<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetNullLatitudeLongitude extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('record_sites', function (Blueprint $table) {
            $table->string('latitud')->nullable()->change();
            $table->string('longitud')->nullable()->change();
        });
        Schema::table('record_homes', function (Blueprint $table) {
            $table->string('latitud')->nullable()->change();
            $table->string('longitud')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_sites', function (Blueprint $table) {
            //
        });
    }
}
