<?php

use Illuminate\Database\Seeder;

class AffectationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('affectation_types')->insert(
            [
                [ 'name' => 'Inundación'],
                [ 'name' => 'Colapso de edificaciones'],
                [ 'name' => 'Incendio de viviendas'],
                [ 'name' => 'Incendio de un sector'],
                [ 'name' => 'Caida de Arbol'],
                [ 'name' => 'Deslizamiento de suelos y/o Rocas'],
                [ 'name' => 'Incendio Estructural'],
                [ 'name' => 'Desechos en via pública'],
                [ 'name' => 'Accidente de tránsito'],
                [ 'name' => 'Daño en redes de servicio públicos alcantarillado'],
                [ 'name' => 'Daño en redes de servicio públicos acueducto'],
                [ 'name' => 'Daño en redes de energia'],
                [ 'name' => 'Derrame de sustancias químicas peligrosas'],
                [ 'name' => 'Escape de gas'],
                [ 'name' => 'Vendaval'],
                [ 'name' => 'Creciente subita'],
                [ 'name' => 'Erosión Costera'],
                [ 'name' => 'Avenidas torrenciales'],
                [ 'name' => 'Escorrentia superficial'],
                [ 'name' => 'Incendio por cobertura vegetal']
            ]
        );
    }
}
