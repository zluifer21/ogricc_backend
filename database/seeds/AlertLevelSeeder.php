<?php

use Illuminate\Database\Seeder;

class AlertLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alert_levels')->insert(
            [
                [ 'name' => 'Roja'],
                [ 'name' => 'Naranja'],
                [ 'name' => 'Amarilla'],
                [ 'name' => 'Verde'],
                [ 'name' => 'No Aplica']
            ]
        );
    }
}
