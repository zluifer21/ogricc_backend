<?php

use Illuminate\Database\Seeder;

class CharacteristicPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('characteristics_place')->insert(
            [
                [ 'name' => 'Sector Residencial'],
                [ 'name' => 'Sector institucional'],
                [ 'name' => 'Sector turistico'],
                [ 'name' => 'Vias de la ciudad'],
                [ 'name' => 'Sector de interes ambiental'],
                [ 'name' => 'Mar']

            ]
        );
    }
}
