<?php

use Illuminate\Database\Seeder;

class FloorTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('floor_types')->insert(
            [
                [ 'name' => 'Pendientes pronuncadas'],
                [ 'name' => 'Sin pendientes pronuncadas']
            ]
        );
    }
}
