<?php

use Illuminate\Database\Seeder;

class IdentificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('identification_types')->insert(
            [
                [ 'name' => 'Cédula'],
                [ 'name' => 'Pasaporte']
            ]
        );
    }
}
