<?php

use Illuminate\Database\Seeder;

class MediaTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_types')->insert(
            [
                [ 'name' => 'Imagen'],
                [ 'name' => 'Video']
            ]
        );
    }
}
