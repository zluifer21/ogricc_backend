<?php

use Illuminate\Database\Seeder;

class RecordHomeUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert(
            [
                [ 'name' => 'Nínguno']
            ]
        );
        DB::table('helps')->insert(
            [
                [ 'name' => 'Nínguno']
            ]
        );
    }
}
