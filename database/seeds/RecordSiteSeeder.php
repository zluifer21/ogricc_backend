<?php

use App\Models\Raitig;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
class RecordSiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,1000) as $index) {
            $user= \App\User::all()->random(2)->first();
            $characteristics_place = \App\Models\CharacteristicPlace::all()->random(2)->first();
            $neighborhood= \App\Models\Neighborhood::all()->random(2)->first();
            $alertlevel = \App\Models\AlertLevel::all()->random(2)->first();
            $risk=\App\Models\RiskZone::all()->random(2)->first();
            $floor=\App\Models\FloorType::all()->random(2)->first();
            $object= \App\Models\Recordsite::create([
                'date'=>$faker->dateTimeBetween('-30 days')->format('Y-m-d'),
                'neighborhood_id'=>$neighborhood->id,
                'latitud'=>$faker->randomFloat(),
                'longitud'=>$faker->randomFloat(),
                'characteristics_place_id'=>$characteristics_place->id,
                'alert_level_id'=>$alertlevel->id,
                'duration'=>$faker->biasedNumberBetween(1,10),
                'n_families'=>$faker->biasedNumberBetween(0,450),
                'n_homes'=>$faker->biasedNumberBetween(0,300),
                'n_schools'=>$faker->biasedNumberBetween(0,1),
                'n_healthcares'=>$faker->biasedNumberBetween(0,1),
                'n_person_affecteds'=>$faker->biasedNumberBetween(1,2000),
                'risks_zone_id'=>$risk->id,
                'floor_type_id'=>$floor->id,
                'n_person_wounds'=>$faker->biasedNumberBetween(0,100),
                'n_person_deads'=>$faker->biasedNumberBetween(0,10),
                'observations'=>$faker->paragraph,
                'user_id'=>$user->id,
                'time'=>$faker->time(),
                'created_at'=>$faker->dateTime()
            ]);

            $geograficas=Raitig::where('raiting_type',1)->get();
            $ambientales=Raitig::where('raiting_type',2)->get();
            $sociales=Raitig::where('raiting_type',3)->get();
            $emergencias=Raitig::where('raiting_type',4)->get();
            $institucionales=Raitig::where('raiting_type',5)->get();
            $territoriales=Raitig::where('raiting_type',6)->get();
            $num=$faker->biasedNumberBetween(1,2);
            $afectattios=\App\Models\AffectationType::all()->random($num)->pluck('id');
            $object->affectationType()->attach($afectattios);
            $object->rating()->attach($territoriales->random(2)->first()->id);
            $object->rating()->attach($institucionales->random(2)->first()->id);
            $object->rating()->attach($sociales->random(2)->first()->id);
            $object->rating()->attach($ambientales->random(2)->first()->id);
            $object->rating()->attach($emergencias->random(2)->first()->id);
            $object->rating()->attach($geograficas->random(2)->first()->id);

        }
    }
}
