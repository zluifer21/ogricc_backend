<?php

use Illuminate\Database\Seeder;

class RiskZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('risks_zone')->insert(
            [
                [ 'name' => 'Cerca'],
                [ 'name' => 'Sobre ronda hidrica'],
                [ 'name' => 'No se encuentra']
            ]
        );
    }
}
