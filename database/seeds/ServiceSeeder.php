<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert(
            [
                [ 'name' => 'Agua'],
                [ 'name' => 'Luz'],
                [ 'name' => 'Gas']
            ]
        );
    }
}
