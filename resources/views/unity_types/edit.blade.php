@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Unity Type
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($unityType, ['route' => ['unityTypes.update', $unityType->id], 'method' => 'patch']) !!}

                        @include('unity_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection