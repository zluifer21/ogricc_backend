<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return Auth::user()->load('roles');

});

Route::get('dashboard', 'DashboarController@index');

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('locations', 'LocationAPIController');
    Route::resource('neighborhoods', 'NeighborhoodAPIController');
    Route::resource('areas', 'AreaAPIController');
    Route::resource('identification_types', 'IdentificationTypesAPIController');
    Route::resource('people', 'PersonAPIController');
    Route::resource('information', 'InformationAPIController');
    Route::resource('person_informations', 'PersonInformationAPIController');
    Route::resource('characteristic_places', 'CharacteristicPlaceAPIController');
    Route::resource('alert_levels', 'AlertLevelAPIController');
    Route::resource('risk_zones', 'RiskZoneAPIController');
    Route::resource('floor_types', 'FloorTypeAPIController');
    Route::resource('affectation_types', 'AffectationTypeAPIController');
    Route::resource('rating_types', 'RatingTypeAPIController');
    Route::resource('raitigs', 'RaitigAPIController');
    Route::resource('media_types', 'MediaTypeAPIController');
    Route::resource('media', 'MediaAPIController');
    Route::resource('recordsites', 'RecordsiteAPIController');
    Route::get('neighborhoods-filter','NeighborhoodAPIController@filter');
    Route::get('recordsites-create','RecordsiteAPIController@init');
    Route::resource('bitacoras', 'BitacoraAPIController');
    Route::resource('genders', 'GenderAPIController');
    Route::resource('sites', 'SiteAPIController');
    Route::resource('property_types', 'PropertyTypeAPIController');
    Route::resource('unity_types', 'UnityTypeAPIController');
    Route::resource('home_statuses', 'HomeStatusAPIController');
    Route::resource('materials', 'MaterialAPIController');
    Route::resource('helps', 'HelpAPIController');

    Route::resource('users', 'UserController');
    Route::get('roles', 'RolesController@index');
    Route::get('areas-user', 'AreaAPIController@users');
    Route::get('recordhome-create','RecorHomeAPIController@init');
    Route::get('me','UserController@me');
    Route::get('recor_homes_clone','RecorHomeAPIController@clone');
    Route::get('recor_sites_clone','RecordsiteAPIController@clone');
    Route::get('recor_homes_export','RecorHomeAPIController@export');
    //Route::get('recor_sites_export','RecordsiteAPIController@export');
    Route::resource('recordsites', 'RecordsiteAPIController');
    Route::resource('recor_homes', 'RecorHomeAPIController');
    Route::get('migrate', 'MigrateController@index');
    Route::get('chart_homes','RecorHomeAPIController@chart');
    Route::get('recor_sites_export','RecordsiteAPIController@export');


});




